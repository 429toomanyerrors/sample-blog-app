from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView


try:
    from posts.models import Post
except Exception:
    Post = None


# Create your views here.
# def show_post_detail(request, pk):
#    if Post:
#       post = Post.objects.get(pk=pk)
#  else:
#     post = None
# context = {
#    "post": post,
# }
# return render(request, "posts/detail.html", context)


# def list_all_posts(request):
#   if Post:
#      posts = Post.objects.all()
# else:
#    posts = None
# context = {
#   "posts": posts,
# }
# return render(request, "posts/list.html", context)


class PostListView(ListView):
    model = Post
    context_object_name = "posts"
    template_name = "posts/list.html"
    paginate_by = 3

    def get_queryset(self):
        query = self.request.GET.get("q")
        if not query:
            query = ""
        return Post.objects.filter(title_incontrains=query)


class PostDetialView(DetailView):
    model = Post
    context_object_name = "post"
    template_name = "posts/detail.html"

    def get_queryset(self):
        query = self.request.GET.get("q")
        if not query:
            query = ""
        return Post.objects.filter(title_incontrains=query)


class PostCreateView(CreateView):
    model = Post
    fields = ["title", "content", "created_at"]
    template_name = "posts/create.html"
    success_url = "/"


class PostUpdateView(UpdateView):
    model = Post
    fields = ["title", "content", "created_at"]
    template_name = "posts/update.html"
    success_url = "/"
