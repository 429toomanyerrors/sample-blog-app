from django.urls import path

from posts.views import (
    PostDetailView,
    PostListView,
    PostUpdateView,
    PostCreateView,
)

urlpatterns = [
    path("<int:pk>/", PostDetailView.as_view(), name="PostDetailView"),
    path("", PostListView.as_view, name="PostListView"),
    path("<int:pk/update", PostUpdateView.as_view(), name="post_update"),
    path("create", PostCreateView.as_view(), name="post_create"),
]
